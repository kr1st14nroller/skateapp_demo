package com.example.skateapp.persistencia;

import android.content.Context;
import android.database.Cursor;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skateapp.AdapterArticulos;
import com.example.skateapp.ListArticulos;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DbProductos extends DbHelper {

    Context context; // Variable global
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = firebaseFirestore.getInstance();
    AdapterArticulos adapterArticulos;
    RecyclerView recyclerView;

    // Constructor
    public DbProductos(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public void agregarArticulo(String nombrep, String descripcion, String precio) {
        Map<String, Object> cv = new HashMap<>(); // Instancia
        cv.put("nombrep", nombrep);
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        firebaseFirestore.collection("productos").add(cv);
    }


    public void eliminarArticulo(String id) {
        firebaseFirestore.collection("productos").document(id).delete();
    }

    public void actualizarArticulo(String descripcion, String precio,String id) {
        Map<String, Object> cv = new HashMap<>();
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        firebaseFirestore.collection("productos").document(id).update(cv);
    }

    public List<ListArticulos> consultarArticulos() {
        List<ListArticulos> listArticulos = new ArrayList<ListArticulos>(); // Instancia de un objeto tipo lista
        // Cursor: sirve para  buscar datos por medio de saltos en las columna existentes en la base de datos
        Cursor result = this.getWritableDatabase().query("productos", new String[]{"codigop", "nombrep", "descripcion", "precio"}, null, null, null, null, null);


        Query query = dbf.collection("productos");

        FirestoreRecyclerOptions<ListArticulos> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<ListArticulos>()
                .setQuery(query, ListArticulos.class).build();

        adapterArticulos = new AdapterArticulos(firestoreRecyclerOptions);
        adapterArticulos.notifyDataSetChanged();
        recyclerView.setAdapter(adapterArticulos);

        return listArticulos;
    }
}