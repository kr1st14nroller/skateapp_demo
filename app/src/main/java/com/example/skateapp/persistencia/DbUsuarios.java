package com.example.skateapp.persistencia;

import static android.content.ContentValues.TAG;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.skateapp.MainActivity;
import com.example.skateapp.FormularioRegistro;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class DbUsuarios extends DbHelper{

    Context context; // Variable global
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = FirebaseFirestore.getInstance();
    public boolean check_usuario = false;
    public boolean check_pass = false;
    public boolean usuario_insertado = false;

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        // SUPER: Llama al contructor de la clase padre
        super(context);
        this.context = context;
    }

    // Long: Es de tipo entero de mayor tamaño

    public void insertarUsuario(String nomusuario, String contrasena, String correo, OnSuccessListener metodo, OnFailureListener metodomal) {
        Map<String,String> data = new HashMap<String,String>();
        data.put("nomusuario",nomusuario);
        data.put("contrasena",contrasena);
        data.put("correo",correo);
        dbf.collection("usuarios").document().set(data).addOnSuccessListener(metodo).addOnFailureListener(metodomal);
    }

    public void checknomusuario(String nomusuario, OnCompleteListener metodo){ //
        // NEW
        Query q = dbf.collection("usuarios").whereEqualTo("nomusuario", nomusuario);
        q.get().addOnCompleteListener(metodo);
    }


    public void checkcontrasena(String nomusuario, String contrasena,OnCompleteListener metodo){

        CollectionReference usersRef = dbf.collection("usuarios");

        Query query = dbf.collection("usuarios").whereEqualTo("nomusuario", nomusuario);
        query.get().addOnCompleteListener(metodo);
    }
}


